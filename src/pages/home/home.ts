import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  
    this.myGroup = new FormGroup({
       cpf: new FormControl()
    });
  }

  myGroup;
  public myModel = ''
  public mask = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/,'-', /\d/, /\d/]

  removeMask(input: any){
    return input.replace(/[-_.]/gi, '');
  }

  show(){
    alert("MASK: " + this.myModel
          + "\n" +
          "CLEAR: " + this.removeMask(this.myModel)
        );
  }

}
